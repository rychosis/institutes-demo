Feature: Content
  In order to test some basic Behat functionality
  As a website user
  I need to be able to see that the Drupal and Drush drivers are working

# TODO: 'Given ... content' (below) works, but 'When I am viewing ... content'
# uses data that pantheonssh rejects

#  @api
#  Scenario: Create a node
#    Given I am logged in as a user with the "administrator" role
#    When I am viewing an "article" content with the title "My article"
#    Then I should see the heading "My article"

  @api
  Scenario: Create many nodes
    Given "page" content:
    | title    |
    | Page one |
    | Page two |
    And "article" content:
    | title          |
    | First article  |
    | Second article |
    And I am logged in as a user with the "administrator" role
    When I go to "admin/content"
    Then I should see "Page one"
    And I should see "Page two"
    And I should see "First article"
    And I should see "Second article"

# Setting the body field contents does not seem to be effective

#  @api
#  Scenario: Create nodes with fields
#    Given "article" content:
#    | title                     | promote | body             |
#    | First article with fields |       1 | PLACEHOLDER BODY |
#    When I am on the homepage
#    And follow "First article with fields"
#    Then I should see the text "PLACEHOLDER BODY"

#  @api
#  Scenario: Create and view a node with fields
#    Given I am viewing an "Article" content:
#    | title | My article with fields! |
#    | body  | A placeholder           |
#    Then I should see the heading "My article with fields!"
#    And I should see the text "A placeholder"

  @api
  Scenario: Create users
    Given users:
    | name     | mail            | status |
    | Joe User | joe@example.com | 1      |
    And I am logged in as a user with the "administrator" role
    When I visit "admin/people"
    Then I should see the link "Joe User"
